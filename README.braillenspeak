
To use the Braille 'n Speak server, you must set up the serial port on
your device (9600 N 8 1 with software handshake) and put it into
"speech box mode".  These are the commands for the Braille 'n Speak:

	status menu	p chord (3-4 chord)
	next item	4 chord
	previous item	1 chord
	say again	1-4 chord
	next choice	spacebar
	exit status menu e chord (1-5 chord)

To set one parameter, the experienced user may find it more convenient
to enter the status menu with a p chord (3-4 chord) then issue one of
the following commands.  Each command also exits the status menu, so
to change another parameter, another p chord is required.

	9600 baud		b 9 (dots 1-2, 3-5)
	no parity		p n (dots 1-2-3-4, 1-3-4-5)
	duplex none		d n (dots 1-4-5, 1-3-4-5)
	data bits item		dots 4-5-6
	8 data bits		st-sign-chord, 8 (dots 4-5-6, 2-3-6)
        1 stop bit              s 1 (dots 2-3-4, 2)
	software handshaking 	h s (dots 1-2-5, 2-3-4)

Finally, enter speech box mode as follows:

	speech parameters menu	3-4-5 chord
	speech box mode		1-2-3-4-5-6 chord (all buttons down)
	block handshaking	b (dots 1-2)

When you want to leave speech box mode	

	speech box mode off	1-2-3-4-5-6 chord (all buttons down)
	exit			e chord (1-5 chord)

Jan McSorley <mcsorley@eden.com> notes: "When your BNS is set up to the
serial port, everything that comes across the port is written to
whatever file the BNS happens to be open when you turn the Speech Box
Mode on.  The newer BNS models have an automatic file bigger/smaller
feature so that they can accommodate large amounts of data, but most
BNS models require you to pre-set your file size.  It's probably a
good idea for people to either create a file with a least 2 pages of
storage space, or open their clipboard and make sure that the
clipboard file is at least 2 pages in size."

                            - Jim Van Zandt


