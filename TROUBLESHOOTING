                      TROUBLESHOOTING

 1a. If you have a synthesizer connected to a serial port, set up the
port:

  stty sane 9600 raw -echo crtscts </dev/ttyS1
  stty -echo                       </dev/ttyS1
  stty ixon ixoff                  </dev/ttyS1

(Note - you cannot combine all these in the same command.)

 1b. If you have the DoubleTalk PC (internal DoubleTalk), install the
modular device driver using the instructions it came with.  It should
speak when the driver is installed.

 2. You can check the connection and port number with echo:

	echo $'this is a test\r' >/dev/ttyS1

If this fails, you can obviously try /dev/ttyS0 and other ports.
also, try /dev/cua0.  (maybe this is necessary for older kernels?)

you can similarly test the internal doubletalk like this:

	echo $'this is a test\r' >/dev/dtlk

the "echo" test shows that the synthesizer or speaker is connected
where you think it is.  There are several more things to check:

 3. Does the speech server know where the synthesizer is?  one test is
to type
	set | grep DTK_PORT

If the "tcl doubletalk" command fails, then your DTK_PORT environment
variable may not be set, or is not exported.  Do that with these
commands:
	DTK_PORT=/dev/ttys0
	export DTK_PORT
and repeat the test.  With bash, you can also combine these commands
like this:
	export DTK_PORT=/dev/ttys0

 4. Now check whether tcl is installed and working.  cd to the
directory with the speech servers.  If you have the source
distribution, you know where that is.  If you have installed a binary
package, you can ask the package maintenance program.  On debian, the
servers should be in /usr/share/emacs/site-lisp/emacspeak/servers.
You can start the speech server using tcl but not emacspeak, like
this:
	tcl doubletalk

(of course, substitute "accent" or whatever, as appropriate.)

The server should introduce itself by saying "This is the DoubleTalk
speech server..."  and you will be left at the tcl prompt.  At this
point you can use any of the tcl commands defined in the speech
server.  For example,

    tts_say {hello world}

should say the words.

To return to the shell prompt, use the command "exit".

You can also use the server to speak a single statement like this:

   echo "tts_say {hello world}" | tcl ./doubletalk

 4. The next question is whether emacspeak knows what driver to use.
Using a set command as above, check that the environment variable
DTK_TCL has the value "doubletalk", "accent", or "braillenspeak".  If
not, set it with the commands
	DTK_TCL=doubletalk
	export DTK_TCL

 5. Next, be sure the driver can be found.  cd to
/usr/share/emacs/site-lisp/emacspeak and type
	ls doubletalk accent braillenspeak

If one of the files is not found, you probably did not complete the
installation of the driver.  As root, you need to change to the driver
directory and type the command
	make install

 6. Next, you may not have the startup script in your path.  Type
	which emacspeak

You should get a reply like
	/usr/bin/emacspeak

otherwise, you have not installed the emacspeak package correctly.  If
so, then you should also be able to type
	emacspeak
and start emacs with speech support.

Once you have it working, you will probably want to add appropriate
commands to one of your startup scripts.  For example:
	path=$path:/usr/local/bin
 	DTK_PORT=/dev/ttys0
	export DTK_PORT
	DTK_TCL=doubletalk
	export DTK_TCL

For reference, here's where bash looks for startup command (quoting
from the bash man page):

       When  bash is invoked as an interactive login shell, or as
       a non-interactive shell with the --login option, it  first
       reads and executes commands from the file /etc/profile, if
       that file exists.  After reading that file, it  looks  for
       ~/.bash_profile,  ~/.bash_login,  and  ~/.profile, in that
       order, and reads and executes commands from the first  one
       that  exists  and is readable.

Once you have made these additions, switch to another virtual console
with alt-f2, log in again, and test the setup with
	emacspeak
You can always switch back to the first virtual console with alt-f1.
The different virtual consoles all share the same file system, of
course.  However, they have different processes, and can have
different environment variables and current directories.


 7. If emacspeak gives you the message "process speaker not running",
you need some trace data to diagnose the real problem.  Make sure you
have strace installed, then try running emacspeak like this:

   strace -o log -ff -t -s 80 emacspeak readme

strace logs system calls, which include all the data read or written
by the server.  There is one log file for each process created.  The
first file, for the process executing the emacspeak script, is named
"log".  The other files have names which include the process number,
like log.1180.  To see what process each log comes from, look for the
execve system call near the beginning of the log file.  The
interesting file is the one for the tcl process that executes the
server script, which should include a line like this:

	log.1181:21:08:31 execve("/usr/bin/tcl", ["/usr/bin/tcl", "/usr/share/emacs/site-lisp/emacspeak/dtk-pgm"...], [/* 26 vars */]) = 0

(the emacs process log is normally the biggest one.  The one with the
next higher process id is normally the one for tcl.)  Then, check the
"write" commands in the last 30 or so lines in the file.  Tcl probably
wrote an error message describing the problem.

The script "testit" in the source distribution attempts to automate
this.  (It may be installed in /usr/share/doc/emacspeak-ss/testit.)
It combines deletion of old log files, running emacspeak under strace,
determining which log file came from tcl, and extracting the last few
things it wrote (whether to the speech device, to stdout, or to
stderr).  Its output is written to the file log.tcl .

I have had the system lock up while recording system calls in this
way.  This happened when I went into info mode with C-h I and tried to
access one of the info files.  However, the same commands worked fine
when I was not recording system calls.  I have not experienced lockups
with the latest version of the server.  It is sometimes necessary to
kill the emacspeak process from another virtual terminal, and to type
"reset" on the original virtual terminal to recover the proper screen
display.

Incidently, strace is very valuable for other things too.  The classic
problem it helps solve is where a program reports that it cannot find
a file.  By running it under strace, you can find out the exact file
name and which directories the program was looking in.  You then know
where you can install the file (or a symbolic link).  All this can be
done without access to the source code for the program.
