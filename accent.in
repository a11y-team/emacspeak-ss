# accent.in - Interfacing Emacspeak to an Accent via TCL.    -*-tcl-*-
# Keywords: Emacspeak, Accent, TCL
#
# Original program by T. V. Raman. 
# Modifications for the DoubleTalk by Jim Van Zandt <jrv@vanzandt.mv.com>
# Modifications for the Accent SA and compatibles by Jack Chen
# <jackchen@pa.dec.com>
#
# $Id: accent.in,v 1.10 2006/02/09 00:20:50 jrv Exp jrv $
#
# Author's note: Accent does not have a tone generator, so all references
# to this can be safely ignored.

# {{{ Copyright:  

# Copyright (c) 1995, 1996, 1997 T. V. Raman, Adobe Systems
# Incorporated.
# All Rights Reserved

# Copyright (c) 1994, 1995 by Digital Equipment Corporation.
# All Rights Reserved. 
#
#
#Copyright 1998 Digital Equipment Corporation. This software is a
#research work and is provided "as is." Digital disclaims all express and
#implied warranties with regard to such software, including the warranty
#of fitness for a particular purpose.  In no event shall Digital be
#liable for any special, direct, indirect, or consequential damages or
#any other damages whatsoever, including any loss or any claim for loss
#of data or profits, arising out of the use or inability to use the
#software even if Digital has been advised of the possibility of such
#damages. This software shall not be further distributed without prior
#written permission from Digital Equipment Corporation.


# }}}
# {{{ beginning of Accent - specific functions

proc version {} {
    global tts
    q {this is Accent, unknown version  }
    d
}

#   Return a Accent SA command string to generate a tone with the
#   specified frequency (in Hz) and duration (in msec).

proc tone_command {{frequency 440} {duration 50}} {
    global tts queue

    #todo: accent has no tones
    #want to fake it
    return "beeeep"
}

proc silence_command  {{duration 50}} {
#todo: want to make silences.
#silence pauses not implemented by accent
#we make a space pause increase and space pause decrease for each 10
#duration
    set silence ""
    loop i 0 [expr duration/10] {
	append silence "$tts(silencecmd_begin)"
    }
    append silence " "
    loop i 0 [expr duration/10] {
	append silence "$tts(silencecmd_end)"
    }
    return silence
}

#helper function for rate_command
proc rate_command_initialize {} {
    global rate_command_initialized? 
    global rate_command_ranges
    #This variable gives the lower limits for each words per minute range.
    #if array(1)==100 and we have a value of 150, then 1 is the mapped speech
    #rate for accent.

    #Note for the accent, all speech rates slower than 8 are of no use because
    #there is not much difference and they are extremely slow.

    set rate_command_initialized? 1

    set rate_command_ranges(8) 60
    set rate_command_ranges(9) 100
    set rate_command_ranges(A) 140
    set rate_command_ranges(B) 180
    set rate_command_ranges(C) 230
    set rate_command_ranges(D) 280
    set rate_command_ranges(E) 330
    set rate_command_ranges(F) 380
    set rate_command_ranges(G) 480
    set rate_command_ranges(H) 530

    return ""
}

proc return_rate_command_range {r} {
    global rate_command_ranges

    foreach i {H G F E D C B A 9 8} {
	if {$rate_command_ranges($i)<=$r} {
	    return $i
	} 
    } 

    #the rate is lower than our lowest so we return our lowest
    return 0
}

# Return speech rate command for Accent.
# Argument is desired rate in words per minute.
proc rate_command {r} {
    global rate_command_initialized?
    if {${rate_command_initialized?} ==0} {
	rate_command_initialize
    }
    set retval [eval return_rate_command_range {$r}]
    return [format "\033R%s" $retval]
}

# Return punctuation mode command for Accent
proc punctuation_command {} {
    global tts
    global ESC
    set mode  $tts(punctuations) 
    set punctuation(all) "${ESC}OP${ESC}OR"
    set punctuation(some) "${ESC}OP${ESC}Or"
    set punctuation(none) "${ESC}Op"
    return $punctuation($mode)
}


proc tts_initialize {} {
    global tts
    global rate_command_initialized?
    global ESC

    # Accent defaults to 9600 n 7 1
    exec stty cs7 < $tts(port)

    # Accent commands
    set ESC "\033"
    set CTRLF "\006"
    set CTRLX "$ESC=x"
    set rate_command_initialized? 0

    set tts(charmode) "${ESC}Ot"
    set tts(stop) "${ESC}=x"
    set tts(textmode) "${ESC}OT"
    set tts(silencecmd_begin) "${ESC}+S"
    set tts(silencecmd_end) "${ESC}-S"
    set tts(resetcmd) "${ESC}=X"
    set tts(somepunct) "${ESC}OP${ESC}Or"
    set tts(mark) "$CTRLF"
    set tts(flush) "\r"
    set tts(tone_440_10) [tone_command 440 10]
    set tts(tone_660_10) [tone_command 660 10]
    set tts(paul) "${ESC}P0${ESC}V5"
    set tts(henry) "${ESC}P1${ESC}V5"
    set tts(dennis) "${ESC}P2${ESC}V4"
    set tts(frank) "${ESC}P4${ESC}V4"
    set tts(betty) "${ESC}P5${ESC}V6"
    set tts(ursula) "${ESC}P6${ESC}V7"
    set tts(rita) "${ESC}P7${ESC}V7"
    set tts(wendy) "${ESC}P8${ESC}V9"
    set tts(kit) "${ESC}P9${ESC}V9"
    set tts(version) "accent server from emacspeak-ss version @version@"
    set tts(initstring) \
"${ESC}=R${ESC}=A${ESC}=V${ESC}=F${ESC}=OA${ESC}=B${ESC}=M${ESC}=Y${ESC}YX \
This is the Accent SA speech server.\r\
"
}

# }}} end of Accent - specific functions

# {{{ Emacs local variables  

### Local variables:
### major-mode: tcl-mode 
### voice-lock-mode: t
### folded-file: t
### End:

# }}}
