            Support for Other Synthesizers

I have written servers for the synthesizers I have to test with.  I am
happy to incorporate into this package servers contributed by others
(like the Accent support contributed by Jack Chen).

Each server is a Tcl script which reads commands from standard input
and writes to the synthesizer.  The script has a common part, all.in,
and a part like doubletalk.in which is unique to each synthesizer.
The unique part must define these five functions, which are called
from all.in:


tts_initialize
	Initialize a number of synthesizer-unique elements in the
	global array tts.  For example, set the element
	tts(stop) to the synthesizer command to immediately
	stop speech.  tts(paul) should be the command for the
	default voice.

tone_command freq dur
	Return a string containing the synthesizer escape codes to
	generate a tone of frequency freq (in Hertz, default 440) and
	duration dur (in milliseconds, default 50).  If the
	synthesizer cannot generate tones, this may return an empty
	string, or possibly the word "beep".
	
silence_command dur
	Return a string containing the synthesizer escape codes to
	pause speech for duration dur (in milliseconds, default 50).

rate_command wpm
	Return a string with the synthesizer escape codes to set the
	speech rate to wpm (in words per minute).  Any rate should be
	legal.  If the user asks for too slow or fast a rate, just set
	the device to the slowest or fastest supported rate
	respectively.

punctuation_command val
	Return a string with the synthesizer command to set which
	punctuation marks are spoken.  It should depend on the global
	variable tts(punctuations), which takes the values
	"all", "some" and "none".

Put these definitions into a file with a name like foo.in in the
emacspeak-ss source directory, and add the base name ("foo") to the
SERVERS list in Makefile.in .  Build and install the new server with
these commands:

	./configure
	make
	make install

When you have everything working, send me a copy of foo.in (and
README.foo if needed), and I'll add it to the collection.  If you find
it necessary to change something in all.in, let me know.  I'm sure we
can come to some agreement.

                               - Jim Van Zandt

